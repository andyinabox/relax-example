import { component, html, useMemo, useEffect, useState } from '@andyinabox/relax'
import { useContenteditable } from '@andyinabox/relax/hooks'
import { docSaveEvent, docDeleteEvent } from '@andyinabox/relax/events'
import { imageDoc } from '../domain/entities'

import styles from '../styles/shared.css'

export default function DocImage({ doc, authenticated, hasChanges }) {

  const [caption, setCaption, captionHasEdits] = useContenteditable(doc.caption, 'figcaption')
  const [url, setUrl] = useState(doc.url)
  const urlHasEdits = useMemo(() => (url !== doc.url), [url, doc.url])
  const hasEdits = useMemo(() => (urlHasEdits || captionHasEdits), [urlHasEdits, captionHasEdits, hasChanges])

  const remove = () => this.dispatchEvent(docDeleteEvent(doc))
  const revert = () => {
    setCaption(doc.caption)
    setUrl(doc.url)
  }
  const commit = () => {
    const updated = imageDoc.update({
      ...doc,
      caption,
      url,
    });
    this.dispatchEvent(docSaveEvent(updated));
  }
  useEffect(revert, [doc])

  const renderButtons = () => {
    if (!authenticated) return null
    return html`<rlx-edit-doc
      .styles=${styles}
      @delete=${remove}
      @revert=${revert}
      @commit=${commit}
      can-delete
      ?hide-revert=${!hasEdits}
      ?hide-commit=${!hasEdits}
    ></wc-edit-doc>`
  }

  return html`
    <figure>
      <rlx-editable-img
        .styles=${styles}
        src=${url}
        alt=${caption}
        @change=${({ detail }) => setUrl(detail)}
      ></wc-editable-img>
      <figcaption contenteditable>${caption}</figcaption>
      ${renderButtons()}
    </figure>
    <style>
      figure {
        width: 100%;
        padding: 0;
        margin: 0;
        margin-bottom: 1rem;
      }
      figcaption {
        padding: .25em 0;
      }
      img {
        width: 100%;
        background: #ccc;
        min-height: 50vw;
      }
    </style>
  `
}

DocImage.observedAttributes = ['authenticated', 'has-changes']

customElements.define('proto-image', component(DocImage))

