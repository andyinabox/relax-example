import { component, html, useMemo, useState, useEffect } from '@andyinabox/relax'
import { imageDoc } from '../domain/entities'
import { docSaveEvent } from '@andyinabox/relax/events'

import './proto-main'
import './proto-image'

import styles from '../styles/shared.css'

export function App({ state }) {

  const { changes, authenticated } = state

  const [ showLogin, setShowLogin ] = useState(false);
  const changesCount = useMemo(() => changes.length, [changes])

  const onAddImage = () => {
    const doc = imageDoc.create(imageDoc.defaults)
    this.dispatchEvent(docSaveEvent(doc))
  }

  const renderAdd = () => {
    if (!authenticated) return null
    return html`<rlx-add-doc
      .styles=${styles}
      text="Add Image"
      @add=${onAddImage}
    ></wc-add-doc>`
  }

  const renderLogin = () => {
    if (!showLogin) return null
    return html`<rlx-login
      .styles=${styles}
      @close=${() => setShowLogin(false)}
    ></wc-login>`
  }
  useEffect(() => { if (authenticated) setShowLogin(false) }, [authenticated])

  const images = useMemo(() => state.images.map(doc => {
    const hasChanges = !!state.changes.find(d => (d.id === doc._id));
    return html`<proto-image
      .styles=${styles}
      .doc=${doc}
      ?authenticated=${authenticated}
      ?has-changes=${hasChanges}
    ></proto-image>`
  }), [authenticated, state.images, state.changes]);

  return html`
    <main>
      <proto-main ?authenticated=${authenticated} .doc=${state.main}></proto-main>
      <div class="images">
      ${images}
      ${renderAdd()}
      </div>
      ${renderLogin()}
    </main>
    <rlx-admin
      .styles=${styles}
      ?authenticated=${authenticated}
      changes-count=${changesCount}
      @login=${() => setShowLogin(true)}
    ></wc-admin>
  `
}

customElements.define('proto-app', component(App))

