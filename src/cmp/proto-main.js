import { component, html, useEffect, useMemo, useState } from '@andyinabox/relax'
import { useContenteditable } from '@andyinabox/relax/hooks';
import { docSaveEvent } from '@andyinabox/relax/events'

import { mainDoc } from '../domain/entities'

import styles from '../styles/shared.css'

export default function DocMain({ doc, authenticated }) {
  const [title, setTitle, titleHasEdits] = useContenteditable(doc.title, '.title');
  const [content, setContent, contentHasEdits] = useContenteditable(doc.content, '.content');

  const save = () => this.dispatchEvent(docSaveEvent(mainDoc.update({
    ...doc,
    title,
    content,
  })))

  const revert = () => {
    setTitle(doc.title)
    setContent(doc.content)
  }
  useEffect(revert, [doc])

  const hasEdits = useMemo(
    () => (titleHasEdits || contentHasEdits),
    [titleHasEdits, contentHasEdits],
  )

  const renderEditOptions = () => {
    if (!authenticated || !hasEdits) return null
    return html`
      <rlx-edit-doc .styles=${styles} @revert=${revert} @commit=${save}></wc-edit-doc>
    `
  }

  return html`
    <article>
      <h1 class="title" contenteditable>${title}</h1>
      <div class="content" contenteditable>${content}</div>
      ${renderEditOptions()}
    </article>
    <style>
      article {
        margin-bottom: 1rem;
      }
    </style>
  `
}

DocMain.observedAttributes = ['authenticated']

customElements.define('proto-main', component(DocMain))

