import { css } from '../cmp/node_modules/relax'

export default css`
  *, *:before, *:after {
    -webkit-box-sizing: inherit;
    -moz-box-sizing: inherit;
    box-sizing: inherit;
  }
  img {
    display: block;
    margin: 0;
    padding: 0;
  }

  button, input {
    display: inline-block;
    font-family: var(--font-family);
    font-size: 0.75rem;
    padding: .25em .5em;
  }
  input {
    border-radius: none;
    box-shadow: none;
    border: 2px solid black;
  }
  button, input[type="submit"] {
    background: white;
    color: black;
    border: none;
    cursor: pointer;
  }

  button.submit, input[type="submit"] {
    color: white;
    background: black;
  }
  button.delete {
    color: red;
  }
  button[disabled] {
    cursor: default;
    color: #ccc;
  }
  button[disabled].submit, input[type="submit"][disabled] {
    color: white;
    background: #ccc;
  }
`