import { mainDoc } from './entities'

export default {
  main: mainDoc.create(mainDoc.defaults),
  authenticated: false,
  changes: [],
  images: [],
}