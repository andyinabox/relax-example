import { fetchChangedDocs } from '@andyinabox/relax/actions'

export const fetchMain = (store, patch) => async () => {
  try {
    const doc = await store().get('main')
    patch({ main: doc })
    return 
  } catch(e) {
    if (e.status !== 404) throw e
  }
}

export const fetchImages = (store, patch) => async () => {
  const { docs } = await store().find({ selector: { type: 'image' }})
  patch({ images: docs })
}

export const fetchAll = (store, patch) => async () => {
  const promises = [];
  promises.push(fetchMain(store, patch)())
  promises.push(fetchImages(store, patch)())
  promises.push(fetchChangedDocs(store, patch)())
  return Promise.all(promises)
}

export default {
  fetchMain,
  fetchImages,
  fetchAll,
}
