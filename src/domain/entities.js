import { uuid } from "@andyinabox/relax/util"

export const mainDoc = {
  defaults: {
    title: 'Enter title here',
    content: 'Enter description here',
  },
  create(doc) {
    return {
      ...doc,
      _id: 'main',
      createdAt: new Date(),
      updatedAt: new Date(),        
    }
  },
  update(doc) {
    return {
      ...doc,
      updatedAt: new Date(),
    }
  }
}

export const imageDoc = {
  defaults: {
    url: 'https://picsum.photos/800/600',
    caption: 'Enter caption here',
  },
  create(doc) {
    return {
      ...doc,
      _id: `image-${uuid()}`,
      type: 'image',
      createdAt: new Date(),
      updatedAt: new Date(),        
    }
  },
  update(doc) {
    return {
      ...doc,
      updatedAt: new Date(),
    }
  }
}

export const initState = {
  main: mainDoc.create(mainDoc.defaults),
  authenticated: false,
  changes: [],
  images: [],
}

export default {
  mainDoc,
  imageDoc,
}