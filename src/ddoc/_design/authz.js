export default {
  validate_doc_update: function (newDoc, oldDoc, userCtx) {
    // allowed roles
    var allowed = ['_admin', 'editor'];
    // reducer to find a match
    function reducer (acc, role) {
      var found = (allowed.indexOf(role) !== -1)
      return found || acc;
    }
    // run reducer
    var valid = userCtx.roles.reduce(reducer, false);
    if (!valid) throw({ unauthorized: 'please log in' });
  }
}