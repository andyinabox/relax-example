export default {
  admins: {
    names: [],
    roles: ["admin"],
  },
  members: {
    names: [],
    roles: ["public", "editor"],
  }
}