import { render, html, createApp } from '@andyinabox/relax';
import actions from './src/domain/actions';
import initState from './src/domain/state';
import config from './src/config.local'

import '@andyinabox/relax/cmp'
import './src/cmp'

const container = document.getElementById('relax');

const app = createApp({ ...config, initState, actions })
app.watchState((state, actions, patch) => {

  console.log(state);

  const login = async ({ detail: { username, password } }) => {
    return actions.login(username, password)
  }

  render(
    html`<proto-app
      .state=${state}
      @user-login=${login}
      @user-logout=${actions.logout}
      @publish=${actions.publishChangedDocs}
      @revert-all=${actions.revertAllDocs}
      @doc-save=${({ detail: doc }) => actions.saveDoc(doc)}
      @doc-delete=${({ detail: doc }) => actions.removeDoc(doc)}>
    </proto-app>`,
    container,
  )
})
app.watchDocs((doc, actions, patch) => actions.fetchAll())
