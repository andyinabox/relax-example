import { nodeResolve } from '@rollup/plugin-node-resolve';
import { terser } from "rollup-plugin-terser";
import copy from 'rollup-plugin-copy'
import alias from '@rollup/plugin-alias';
import filesize from 'rollup-plugin-filesize';

const terserSettings = terser({
  compress: { drop_console: true },
  format: { comments: false },
})

const copySettings = copy({
  targets: [
    {
      src: 'index.html',
      dest: 'dist',
      transform: (contents) => {
        return contents.toString().replace(
          /pouchdb(\.\w+)?.js/g,
          'pouchdb$1.min.js'
        )
      },
    },
    { src: 'index.css', dest: 'dist' },
  ]
})

export default [
  {
    input: 'index.js',
    output: {
      file: 'dist/index.js',
      format: 'esm'
    },
    plugins: [
      alias({
        entries: [
          { find: './src/config.local', replacement: './src/config.production' },
        ],
      }),
      nodeResolve(),
      terserSettings,
      copySettings,
      filesize(),
    ]
  }
]